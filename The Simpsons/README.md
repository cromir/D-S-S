# The Simpsons

[Home](../README.md) > The Simpsons

![Image](https://upload.wikimedia.org/wikipedia/en/0/0d/Simpsons_FamilyPicture.png)

* [Easy Rules](Easy.md)
* [Advanced Rules](Advanced.md)
* [Alcoholic Rules](Alcoholic.md)
