# The Simpsons

[Home](../README.md) > [The Simpsons](README.md) > Easy Rules

![Image](https://upload.wikimedia.org/wikipedia/en/0/0d/Simpsons_FamilyPicture.png)

## Easy Rules

Note the Intro only counts for intro specific rules

* You drink 1 everytime you skip the intro
* You drink 1 everytime Moe pours a beer
* You drink 1 everytime Barny burps
* You drink 1 everytime Marge gets angry because of a stupid actio Homer did
* You drink 1 everytime Maggie makes a "non baby action" (climbing out of her bed doesnt count)
* You drink 1 everytime if somebody dies
* You drink 1 everytime Sideshow Bob tries to kill Bart
* You drink 1 everytime Homer sais "Dough"
* You drink 1 everytime Ned sais "Okeli Dokeli"
* You drink 1 everytime Homer sais "Dough"
* You drink 3 everytime a Treehouse of Horror episode is shown
