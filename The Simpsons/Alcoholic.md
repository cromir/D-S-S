# The Simpsons

[Home](../README.md) > [The Simpsons](README.md) > Alcoholic Rules

![Image](https://upload.wikimedia.org/wikipedia/en/0/0d/Simpsons_FamilyPicture.png)

## Alcoholic Rules

* You drink 1 everytime you skip the intro
* You drink 1 everytime Moe pours a beer
* You drink 1 everytime Barny burps
* You drink 1 everytime the rector sais "Skiiiinneeeer"
* You drink 1 everytime MR. Bruns sais "Release the hounds"
* You drink 1 everytime Homer drinks a beer
* You drink 1 everytime Bart sais "Eat my shorts"
* You drink 2 everytime when there is a future prediction shown
* You drink 1 everytime grandpa Simpson tells a story
* You drink 1 everytime Scratchy dies
* You drink 1 everytime there is a problem in the nuclear reactor
* You drink 1 everytime if somebody dies
* You drink 1 everytime a Buzz Cola is consumed
* You drink 1 everytime Homer sleeps at work
* You drink 1 everytime Lisa plays the Sax
* You drink 1 everytime Maggie makes a "non baby action" (climbing out of her bed doesnt count)
* You drink 1 everytime you see the Duff brand
* You drink 1 everytime Marge gets angry because of a stupid actio Homer did
* You drink 1 everytime Sideshow Bob tries to kill Bart
* You drink 1 everytime Homer sais "Dough"
* You drink 1 everytime Ned sais "Okeli Dokeli"
* You drink 3 everytime a Treehouse of Horror episode is shown
* You drink 1 everytime Millhouse loses his glasses
* You drink 1 everytime the school bullies beat up somebody
* You drink 1 everytime Bart has to detent
