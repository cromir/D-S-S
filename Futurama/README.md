# Futurama

[Home](../README.md) > Futurama

![Image](https://upload.wikimedia.org/wikipedia/de/thumb/4/4e/Futurama-logo.svg/743px-Futurama-logo.svg.png)

* [Easy Rules](Easy.md)
* [Advanced Rules](Advanced.md)
* [Alcoholic Rules](Alcoholic.md)
